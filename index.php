<?php
if(isset($_POST)){
  if(!empty($_POST)){
    $query = $_POST['search_key'];
    $category = $_POST['option'];
    $sort = $_POST['sort'];
// API request variables
$endpoint = 'http://svcs.ebay.com/services/search/FindingService/v1';  // URL to call
$version = '1.0.0';  // API version supported by your application
$appid = 'mobiflyi-4065-4920-ba5a-42a7c025ce25';  // Replace with your own AppID
$globalid = 'EBAY-IN';  // Global ID of the eBay site you want to search (e.g., EBAY-DE)
$safequery = urlencode($query);  // Make the query URL-friendly

// Construct the findItemsByKeywords HTTP GET call 
$apicall = "$endpoint?";
$apicall .= "OPERATION-NAME=findItemsAdvanced";
$apicall .= "&SERVICE-VERSION=$version";
$apicall .= "&SECURITY-APPNAME=$appid";
$apicall .= "&GLOBAL-ID=$globalid";
$apicall .= "&keywords=$safequery";
$apicall .= "&paginationInput.entriesPerPage=50";
$apicall .= "&sortOrder=$sort";
$apicall .= "&categoryId=$category";

// Load the call and capture the document returned by eBay API
$resp = simplexml_load_file($apicall);
// Check to see if the request was successful, else print an error
if ($resp->ack == "Success") {
  $results = '';
  // If the response was loaded, parse it and build links  
  foreach($resp->searchResult->item as $item) {
        $pic   = $item->galleryURL;
        $link  = $item->viewItemURL;
        $title = $item->title;
        $price = $item->sellingStatus->currentPrice;
   // For each SearchResultItem node, build a link and append it to $results
    $results .= "<tr><td><img src=\"$pic\" ></td><td><a href=\"$link\">$title</a></td><td>$price.INR</td></tr>";
  }
}
// If the response does not indicate 'Success,' print an error
else {
  $results  = "<h3>Oops! The request was not successful. Make sure you are using a valid ";
  $results .= "AppID for the Production environment.</h3>";
}
}
}
?>

<!-- Build the HTML page with values from the call response -->
<html>
<head>
<title>eBay Search Results</title>
<style type="text/css">body { font-family: arial,sans-serif;} </style>
</head>
<body>
  <h3>Search within e-Bay</h3>
<form method="POST">
  <input name="search_key" type="text" placeholder="Search Keywords" />
<select name="option" title="Select a category for search">                                          
  <option value="" disabled selected>Select Category</option>      
<option value="293">Audio &amp; Home Entertainment</option><option value="267">Books &amp; Magazines</option><option value="178777">Baby &amp; Mom</option><option value="625">Cameras &amp; Optics</option><option value="131090">Cars &amp; Bike Accessories</option><option value="116365">Charity</option><option value="11450">Clothing &amp; Accessories</option><option value="11116">Coins &amp; Notes</option><option value="1">Collectibles</option><option value="178743">eBay Daily</option><option value="13361">Fitness &amp; Sports</option><option value="26395">Fragrances, Beauty &amp; Health</option><option value="1249">Games, Consoles &amp; Accessories</option><option value="11700">Home &amp; Living</option><option value="20710">Home Appliances</option><option value="281">Jewellery &amp; Precious Coins</option><option value="178816">Kitchen &amp; Dining</option><option value="160">Laptops &amp; Computer Peripherals</option><option value="11071">LCD, LED &amp; Televisions</option><option value="162260">Memory Cards, Pen Drives &amp; HDD</option><option value="14416">Mobile Accessories</option><option value="15032">Mobile Phones</option><option value="9800">Motor Classifieds</option><option value="11232">Movies &amp; Music</option><option value="619">Musical Instruments</option><option value="174982">Shoes</option><option value="169977">Stamps</option><option value="92470">Stationery &amp; Office Supplies</option><option value="178741">Tablets &amp; Accessories</option><option value="631">Tools &amp; Hardware</option><option value="220">Toys, Games &amp; School Supplies</option><option value="14324">Watches</option><option value="179545">Warranty Services</option><option value="99">Everything Else</option></select>


<select name="sort" title="Select Sort Order">  <option value="" disabled selected>Select Sort Order</option>      
<option value="EndTimeSoonest">Time:Ending Soonest</option><option value="StartTimeNewest">Time:Newly Listed</option><option value="PricePlusShippingHighest">Price+Shipping:Highest first</option><option value="PricePlusShippingLowest">Price+Shipping:Lowest First</option><option value="DistanceNearest">Distance:Nearest</option><option value="CurrentPriceHighest">Price:Highest First</option></select>
<input type="submit" />
</form>
<hr>
<table>
<tr>
  <td>
    <?php if(isset($_POST)){ if(!empty($_POST)){ echo $results; }} ?>
  </td>
</tr>
</table>
</body>
</html>
